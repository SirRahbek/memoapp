import {createRoot, Root} from "react-dom/client"
import React, {ReactNode} from "react"
import { MemoList } from "./MemoList"
import { MemoView } from "./MemoView"


export class MemoApp extends React.Component<void,void> {
	public constructor () {
		super()
	}


	public render() : React.ReactNode {
		return <div>
			<MemoList/>
			<MemoView/>
		</div>
	}
}


window.onload = () => {
	let memoApp = createRoot( document.querySelector("#memoApp")! )

	memoApp.render( <MemoApp/> )
}
