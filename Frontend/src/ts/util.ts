

//TODO: global state very ugly
//TODO: put in class with a login function
var apiToken : string|null = null

export async function MakeAPICall ( endpoint:string, body:any, callback:(Response)=>void ) : Promise<void> {
	let headers : any = { "Content-Type": "application/json" }

	if (apiToken!=null)
		headers.Authorization = "Bearer " + apiToken

	let request : RequestInit = {
		method  : "POST",
		body    : JSON.stringify(body),
		headers : { "Content-Type": "application/json" },
	}
	
	callback( await fetch("http://localhost:5000"+endpoint, request) )
}
