import {MakeAPICall} from "./util"


window.onload = () => {
	let form = document.querySelector(".loginForm")
	if (form == null)
		throw new Error("could not find form!")
	form.addEventListener("submit", login)
}


async function login ( e : Event ) : Promise<void> {
	if (e.preventDefault)
		e.preventDefault()

	let email    : string = getValue(".loginForm .email")
	let password : string = getValue(".loginForm .password")

	//TODO: Password should be securely hashed clientside
	MakeAPICall("/login", {email,password}, async (res:Response) => {
		if (res.status != 200) {
			wrongLoginResponse()
			return
		}

		let result = await res.json()
		if (result.success == false) {
			wrongLoginResponse()
			return
		}

		window.location.href = `memos.html?token=${result.token}`
	})
}


function wrongLoginResponse() : void {
	console.log("ruh-oh wrong login")
}


function getValue( query : string ) : string {
	return (document.querySelector(query) as HTMLInputElement).value
}