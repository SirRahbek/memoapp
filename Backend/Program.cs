﻿using System.ComponentModel.DataAnnotations.Schema;
using MemoApp;
using Microsoft.AspNetCore;

var webAppBuilder = WebApplication.CreateBuilder(args);

webAppBuilder.Services.AddCors(options => {
	options.AddPolicy(
		name: "AllowList1",
		policy => policy
			.WithOrigins("http://localhost:5000", "http://localhost:1234")
			.AllowAnyHeader()
			.AllowAnyMethod()
	);
});

webAppBuilder.Services.AddSingleton<LoginProvider>();
webAppBuilder.Services.AddSingleton<MemoProvider>();

var webApp = webAppBuilder.Build();

LoginController loginController = new LoginController();
MemoController  memoController  = new MemoController();

webApp.UseCors("AllowList1");
webApp.MapPost( "/login",        loginController.Login      );
webApp.MapPost( "/memo/getlist", memoController.GetMemoList );
webApp.MapPost( "/memo/get",     memoController.GetMemo     );

webApp.Run();