
namespace MemoApp;


public class LoginProvider
{
	//TODO: this ofc should be put in a db
	private static readonly List<User> users = new List<User>{
		new User(1, "test1@internet.com", "1234"),
		new User(2, "test2@internet.com", "1234"),
		new User(3, "test3@internet.com", "1234")
	};

	private List<UserSession> userTokens = new List<UserSession>();


	public (User?, string) Authenticate (string email, string password)
	{
		User? user = users.FirstOrDefault( u => u.Email == email );
		if (user==null)
			return (null, "");
		if (user.PasswordHash != password)
			return (null, "");

		string userHash = UniqueKey.KeyGenerator.GetUniqueKey(32);

		userTokens.Add( new UserSession(user, userHash, DateTime.UtcNow) );

		return (user, userHash);
	}


	public User? GetUserFromToken (string token)
	{
		UserSession? session = userTokens.FirstOrDefault(t => t.token==token);
		if(session==null)
			return null;

		return session.user;
	}


	public User? GetUserFromRequest (HttpRequest request)
	{
		request.Headers.TryGetValue("Authorization", out var authHeader);
		string? authValue = authHeader.FirstOrDefault();
		Console.WriteLine($"authValue: {authValue}");
		if (authValue==null)
			return null;
		if (!authValue.StartsWith("Bearer "))
			return null;
		
		authValue = authValue.Remove(0, 7); //chop "Bearer " from string

		return GetUserFromToken(authValue);
	}


	internal record UserSession(User user, string token, DateTime creationTime);
}