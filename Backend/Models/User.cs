
namespace MemoApp;

public record User (int Id, string Email, string PasswordHash);