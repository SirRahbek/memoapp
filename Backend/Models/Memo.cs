
namespace MemoApp;

public record Memo (int id, int userId, string Name, string Content);