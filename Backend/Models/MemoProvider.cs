
namespace MemoApp;


public class MemoProvider
{
	//TODO: this data should obviously be fetched from a db
	public List<Memo> memos = new List<Memo>{
		new Memo(1, 1, "new memo", "buy milk; sell house; ...; profit!"),
		new Memo(2, 2, "cool memo", "testing testing, 123"),
		new Memo(3, 2, "cool memo 2", "testing testing, 1234576")
	};


	public List<Memo> GetMemosForUser (User user)
	{
		return memos.Where(m => m.userId==user.Id).ToList();
	}


	public Memo? GetMemo (User user, int memoId)
	{
		return memos.FirstOrDefault(m => m.id == memoId);
	}
}