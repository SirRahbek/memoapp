using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Identity.Data;
using Microsoft.AspNetCore.Mvc;

namespace MemoApp;



public class LoginController
{
	public LoginController () {}


	public object Login (HttpContext context, LoginRequest request, LoginProvider loginProvider)
	{
		var (user, userToken) = loginProvider.Authenticate(request.email, request.password);
		if (user==null)
			return new LoginResponse(false, "");

		context.Response.Headers.Append("Authorization", $"Bearer {userToken}");

		return new LoginResponse(true, userToken);
	}
}


public record LoginRequest (string email, string password);
public record LoginResponse (bool success, string token);