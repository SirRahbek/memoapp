
namespace MemoApp;


public class MemoController
{
	public object GetMemoList (HttpContext context, LoginProvider loginProvider, MemoProvider memoProvider)
	{
		User? user = loginProvider.GetUserFromRequest(context.Request);
		if (user == null)
			return new MemoListResponse(null);

		var memoList = memoProvider.GetMemosForUser(user);
		var memoHandleList = memoList.Select( m => new MemoHandle(m.id, m.Name, m.Content.Length) ).ToList();

		return new MemoListResponse(memoHandleList);
	}


	public object GetMemo (HttpContext context, LoginProvider loginProvider, MemoProvider memoProvider, GetMemoRequest request)
	{
		User? user = loginProvider.GetUserFromRequest(context.Request);
		if (user == null)
			return new MemoListResponse(null);

		var memo = memoProvider.GetMemo(user, request.id);

		return new GetMemoResponse(memo);
	}

	public record GetMemoRequest(int id);
	public record GetMemoResponse(Memo memo);
	record MemoListResponse(List<MemoHandle>? memos);
	record MemoHandle (int id, string name, int size);
}